#include <stdio.h>

int main()
{
    int count = 0;
    char sentence[500] , character;

    printf("Enter the string: ");
    fgets(sentence , sizeof(sentence) , stdin);

    printf("Enter the character: ");
    scanf("%c" , &character);

    for(int i=0 ; sentence[i]!= '\0' ; i++)
    {
        if(character == sentence[i])
             ++count;
    }
    printf("Frequency of %c is %d ", character , count);
    return 0;

}
