#include <stdio.h>
#include <string.h>

void reverse(char  sentence[])
{
    int len= strlen(sentence);
    printf("The reverse sentence is: ");
    for(int x = len; x>=0; x--)
    {
        printf("%c" , sentence[x]);
    }
}

int main()
{
    char sentence[150];
    printf("Enter the sentence: ");
    fgets(sentence , 150 , stdin);
    reverse(sentence);
}
